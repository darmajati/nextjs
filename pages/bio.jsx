import { useRef, useState } from "react"

export default function bio(){
    const nameRef = useRef()
    const [name, setName] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()

        setName(nameRef.current.value)
    }
    return(
        <form onSubmit={handleSubmit}>
            <h1>Nama kamu adalah: {name}</h1>

            <input type="text" ref={nameRef} placeholder="Nama Kamu" />
            <button type="submit">Submit</button>
        </form>
    )
}