import { useRouter } from "next/router";
import { useEffect } from "react";

export default function Single(){
    const router = useRouter()
    const id = router.query.id

    const initData = async() => {
        const response = await fetch(`https://dummyjson.com/posts/${id}`)
    }

    useEffect(() => {

    }, [])

    return(
        <>
        <h1>This is single user</h1>
          <p>{id}</p>  
        </>
    )
}