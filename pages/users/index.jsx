import { useEffect, useState } from "react";

export default function Index() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const initData = async () => {
      const response = await fetch("https://dummyjson.com/posts?limit=10");
      const data = await response.json();
      setUsers(data.posts);
    };
    initData();
  }, []);

  return (
    <>
      {users.map((user) => (
        <a href={`/users/${user.id}`} key={user.id}>
          <h1>{user.title}</h1>
        </a>
      ))}
    </>
  );
}
