import { useRouter } from "next/router";

export default function DetailRole(){
    const router = useRouter()

    const {idUsers, idRole} = router.query

    return(
        <>
        <h1>ID Users:{idUsers}</h1>
        <h2>ID Role: {idRole} </h2>
        </>
    )
}