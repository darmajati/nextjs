import { useEffect, useState } from "react";

export default function Index() {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const initData = async () => {
      const response = await fetch("https://dummyjson.com/posts?limit=10");
      const data = await response.json();
      setBlogs(data.posts);
    };
    initData();
  }, []);

  return (
    <>
      {blogs.map((blog) => (
        <a href={`/blog/${blog.id}`} key={blog.id}>
          <h1>{blog.title}</h1>
        </a>
      ))}
    </>
  );
}
