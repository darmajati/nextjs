import React from 'react';
import { useSelector } from 'react-redux';

function Result() {
  const result = useSelector(state => state.calculator.result);

  return <div>Result: {result}</div>;
}

export default Result;
