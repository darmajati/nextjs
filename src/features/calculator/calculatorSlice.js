import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  number1: 0,
  number2: 0,
  result: 0,
};

export const calculatorSlice = createSlice({
  name: 'calculator',
  initialState,
  reducers: {
    setNumber1: (state, action) => {
      state.number1 = action.payload;
    },
    setNumber2: (state, action) => {
      state.number2 = action.payload;
    },
    add: (state) => {
      state.result = state.number1 + state.number2;
    },
    subtract: (state) => {
      state.result = state.number1 - state.number2;
    },
    multiply: (state) => {
      state.result = state.number1 * state.number2;
    },
    divide: (state) => {
      state.result = state.number1 / state.number2;
    },
  },
});

export const { setNumber1, setNumber2, add, subtract, multiply, divide } =
  calculatorSlice.actions;

export default calculatorSlice.reducer;
