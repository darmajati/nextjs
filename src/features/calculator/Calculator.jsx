import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setNumber1,
  setNumber2,
  add,
  subtract,
  multiply,
  divide,
} from './calculatorSlice';

function Calculator() {
  const dispatch = useDispatch();
  const number1 = useSelector(state => state.calculator.number1);
  const number2 = useSelector(state => state.calculator.number2);

  const handleNumber1Change = e => {
    dispatch(setNumber1(Number(e.target.value)));
  };

  const handleNumber2Change = e => {
    dispatch(setNumber2(Number(e.target.value)));
  };

  const handleAddClick = () => {
    dispatch(add());
  };

  const handleSubtractClick = () => {
    dispatch(subtract());
  };

  const handleMultiplyClick = () => {
    dispatch(multiply());
  };

  const handleDivideClick = () => {
    dispatch(divide());
  };

  return (
    <div>
      <input type="number" value={number1} onChange={handleNumber1Change} />
      <input type="number" value={number2} onChange={handleNumber2Change} />
      <button onClick={handleAddClick}>+</button>
      <button onClick={handleSubtractClick}>-</button>
      <button onClick={handleMultiplyClick}>*</button>
      <button onClick={handleDivideClick}>/</button>
    </div>
  );
}

export default Calculator;
